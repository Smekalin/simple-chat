from flask import Flask, render_template, redirect, make_response, request, session
from flask_socketio import SocketIO, send

app = Flask(__name__)
app.config["SECRET_KEY"] = 'vottakoivotsecretkey'
sio = SocketIO(app)

@sio.on('message')
def handle_message(msg):
  print('User:', session['username'])
  print('Message:', msg)
  send({'username': session['username'], 'msg': msg}, broadcast=True)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/chat')
def chat():
  return render_template('chat.html')

@app.route('/login') # принимаем имя пользователя и прописываем в session и отправляем в чат
def login():
  session['username'] = request.args.get('username')
  response = make_response(redirect('/chat'))
  return response



if __name__=='__main__':
  sio.run(app)