Перед запуском вам нужен flask и flask-socketio. Для их скачивания введите следующие команды:
```
pip install flask
```

```
pip install flask-socketio
```

В терминале перейдите в папку проекта simple-chat и пропишите:
```
export FLASK_APP=main.py
```
Проверить получилось или нет можно так:
```
echo $FLASK_APP
```
Затем запустите само приложение:
```
python3 -m flask run
```

Либо можно прописать полный путь во FLASK_APP до main.py и запускать приложение с любой директории